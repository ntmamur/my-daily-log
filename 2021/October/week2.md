### Journal Of Week
- ### <b> Oct-5-2021 - Tuesday <br>
    * kollect-nomadcamp-front page navigation finished
    * Zoom Meeting with 유관식 수석님
      * We have discussed about <i>kollectie type</i> and <i>stage roles</i> for nomadcamp kollection
      * I got feedback that drama-front must be completely finished and tested before using in kollection
    * Fixed drama-dramatic-front storybook and library building  error that says 
  ```React module not found```
    <br>
    <br>
  <b>Useful knowledge:</b><br>
   - yarn script for fixing eslint errors
    ````
    "lint": "eslint --fix --ext .ts,.tsx ."
    ```` 
    * Estimated ````kollectie-type```` and ````stage-roles````<br>
    <img src="img/dramatic-kollectie.png"/> <br>
    <br>
    ###
<br><br><br><br>
- ### <b> Oct-6-2021 - Wednesday<br>
  * drama-dramatic-front library build and publish to register(repository)
  * In ```` drama-dramatic```` backend project id field in mongostore document is not mapping correctly to cdo object. For example, in Project entity there are ````actorKey```` and ````site```` fields which are instance of ````IdName```` class. So, ````id```` field in ````actorKey```` and ````site```` are not mapping. Their values are always null. 
  It seems Spring Data has nuance(edge case) for id field for mongo.
  [Ref link] (https://docs.spring.io/spring-data/data-mongodb/docs/current/reference/html/#mongo-template.id-handling)

* Zoom meetig with 대표님 about kollectie-type and stage-roles for  ````kollect-nomadcamp-front```` 
    * 대표님 explained and designed kollectie-types and stage-roles for kollection
    * At the end of zoom meeting, we decided to use following kollectie-types and stagee-roles:
     ```` 
     TO-BE-EDITED WITH SCHEMA
     ````
     <br><br><br><br>
### <b> Oct-7-2021 - Thursday<br>
  * kollectie-types and stage-roles has been updated for ````kollect-nomadcamp-front```` 
    <br><br><br><br>
### <b> Oct-8-2021 - Friday <br>
  * *Cdo entities fixed because of Id field in composite class fields
  * NARA Platform structure review
<br><br><br><br>
### <b>Oct-9-2021 - Saturday<br>
  * ```drama-dramatic-front``` code error fix according to ``` 